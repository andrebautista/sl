import React, { Component } from 'react';
import { Provider } from 'react-redux';
import Product from './product';

const App = ({ store }) => (
  <Provider store={store}>
    <Product />
  </Provider>
);

export default App;