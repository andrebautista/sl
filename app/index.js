import React from 'react';
import { render } from 'react-dom';
import App from './app';
import configureStore from './configureStore'
const { store } = configureStore();

render(
  <App store={store}/>,
  document.getElementById('app')
);