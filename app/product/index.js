import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getProductData, updateProductData } from './productActions.js';
import styles from './product.scss';
class Product extends Component {
  componentDidMount() {
    getProductData().then(data => (this.props.updateProductData(data)))
  }

  render() {
    let product_data = this.props.product_data;
//    let y_axis_data = d3.scaleLinear()
//      .domain(d3.extent(product_data.sales, function(d) { return d.retailSales });
//      .range([500, 0])
//
//    let x_axis_data = d3.scaleTime()
//        .domain(d3.extent(product_data.sales, function(d) { return d.weekEnding })
//        .range([0, 900]);
//
//    let xAxis = d3.axisBottom(x_axis_data).ticks

    return (
      <React.Fragment>
        {product_data.length > 0 ?
          <section>
            <header className="product-data-labels">
              <div>Week ending</div>
              <div>Retail Sales</div>
              <div>Wholesale Sales</div>
              <div>Units Sold</div>
              <div>Retailer Margin</div>
            </header>
            {product_data[0].sales.map((product, idx) => (
              <article key={idx} className="product-data-row">
                <div>{product.weekEnding}</div>
                <div>{product.retailSales}</div>
                <div>{product.wholesaleSales}</div>
                <div>{product.unitsSold}</div>
                <div>{product.retailerMargin}</div>
              </article>
            ))}
          </section>
        : null
        }
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  product_data: state.data
});

const mapDispatchToProps = {
  updateProductData
};

export default connect(mapStateToProps, mapDispatchToProps)(Product);