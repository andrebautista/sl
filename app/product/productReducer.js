const initProductData = {
  data: [],
};

export const productReducer = (state = initProductData, action) => {
  switch (action.type) {
    case "UPDATE_DATA":
      return {
        ...state,
        data: action.data
      };
    default:
      return state;
  }
};