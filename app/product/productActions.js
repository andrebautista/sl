export const getProductData = () => (
  fetch('https://s3-us-west-2.amazonaws.com/bautista-sl/Webdev_data.json', {
    method: 'GET',
    mode: 'cors'
  })
  .then(response => response.json())
);


export const updateProductData = (data) => ({
  type: "UPDATE_DATA",
  data
});