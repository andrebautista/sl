import { createStore } from 'redux';
import { productReducer as Product } from './product/productReducer';

export default function configureStore() {
  const store = createStore(Product);
  return { store };
}
