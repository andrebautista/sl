#Bautista FE Assessment
*Code is not optimized for a production environment*

###To run local server:
1. `npm install`
2. `npm run watch`

###To build code:
1. `npm install`
2. `npm run build`

##Prompt
###We are looking for:
1. A solution that resembles the provided mock up closely and implements AT LEAST ONE of either the graph or chart as seen in the mock up
2. A working solution that takes the provided JSON file as output from a stubbed-out API call
3. A solution that implements React and Redux best practices and patterns
4. Clean code

###Assets to be returned via email within the provided time frame:
1. A public repository to view the final code
2. A public URL to view the web page